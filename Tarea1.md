# Caso de estudio 'NIKE'

#### Integrantes:
##### Josué Eliseo Soto Martínez 7690-18-4281
##### Jhonathan Estuardo Ixén Cocón 7690-18-26

# 

#### 1. Evalúe el hecho de que Nike use las fuerzas competitivas y los modelos de la cadena de valor.
>##### Nike implementó en unas de sus prendas GPS, el cual puede enviar la información de entrenamiento a una página web de la cual se pueden extraer los datos en un dispositivo USB, el cual le permitirá rastrear su progreso, establecer metas, ver la ruta diaria y encontrar nuevas rutas excelentes, algo que en el momento de implementar esta nueva prenda sus contrincantes FitBit y Jawbone no contaban con dicha capacidad.

#### 2. ¿Qué estrategias competitivas busca Nike? ¿Cómo se relaciona la tecnología de la información con estas estrategias?
>##### A Nike no le interesa ganar dinero vendiendo la información recopilada por sus dispositivos, esta información podría ser valiosa para otras empresas, sin embargo, lo que Nike desea hacer es fabricar dispositivos geniales que se conecten estrechamente con su propio software


#### 3. ¿En qué sentido es Nike una "compañía tecnológica"? Explique su respuesta
>##### Porque algunos de los productos más recientes que ofrece Nike son en realidad productos de tecnología de información, actualmente a fabricado prendas deportivas las cuales almacenan información acerca de los datos del usuario la cual proporciona retroalimentación y motivación al momento que se necesite. 
>##### Ha hecho alianzas con Apple con el fin de ofrecer programas de fidelización a los usuarios de estas prendas tecnológicas 

#### 4. ¿Qué tanta ventaja tiene Nike sobre sus competidores? Explique su respuesta:
>##### La ventaja que puede tener sobre sus competidores es que Nike ya es lider en el área deportiva, debido a que ofrecen una gama amplia de prendas y accesorios deportivos y ya cuentan con un mercado muy amplio, basta simplemente con sacar al mercado los nuevos productos que las personas querrán tener una por la reputación que la empresa tiene.
