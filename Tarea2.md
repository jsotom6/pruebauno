# Caso de estudio 'NIKE'

#### Integrantes:
##### Josué Eliseo Soto Martínez 7690-18-4281
##### Jhonathan Estuardo Ixén Cocón 7690-18-26

# 

#### 1. Evalúe el hecho de que Nike use las fuerzas competitivas y los modelos de la cadena de valor.
>##### Nike implementó en unas de sus prendas GPS, el cual puede enviar la información de entrenamiento a una página web de la cual se pueden extraer los datos en un dispositivo USB, el cual le permitirá rastrear su progreso, establecer metas, ver la ruta diaria y encontrar nuevas rutas excelentes, algo que en el momento de implementar esta nueva prenda sus contrincantes FitBit y Jawbone no contaban con dicha capacidad.

#### 2. ¿Qué estrategias competitivas busca Nike? ¿Cómo se relaciona la tecnología de la información con estas estrategias?
>##### A Nike no le interesa ganar dinero vendiendo la información recopilada por sus dispositivos, esta información podría ser valiosa para otras empresas, sin embargo, lo que Nike desea hacer es fabricar dispositivos geniales que se conecten estrechamente con su propio software


#### 3. ¿En qué sentido es Nike una "compañía tecnológica"? Explique su respuesta
>##### Porque algunos de los productos más recientes que ofrece Nike son en realidad productos de tecnología de información, actualmente a fabricado prendas deportivas las cuales almacenan información acerca de los datos del usuario la cual proporciona retroalimentación y motivación al momento que se necesite. 
>##### Ha hecho alianzas con Apple con el fin de ofrecer programas de fidelización a los usuarios de estas prendas tecnológicas 

#### 4. ¿Qué tanta ventaja tiene Nike sobre sus competidores? Explique su respuesta:
>##### La ventaja que puede tener sobre sus competidores es que Nike ya es lider en el área deportiva, debido a que ofrecen una gama amplia de prendas y accesorios deportivos y ya cuentan con un mercado muy amplio, basta simplemente con sacar al mercado los nuevos productos que las personas querrán tener una por la reputación que la empresa tiene.

#
# Caso de estudio 'WALMART Y AMAZON'

#### 3-11 Analice a Walmart y Amazon.com usando los modelos de fuerzas competitivas y la cadena de valor
>##### Podemos observar que ambas empresas actualmente cuenta con plataformas que les permiten ofrecer los servicios de ventas y organinización de productos de forma eficiente a diferencia de comercios más pequeños

#### 3-12 Compare los modelos de negocios y las estrategias de negocios de Walmart y Amazon.
>##### Amazon cuenta con una amplia gama de productos, no se enfocan solo en productos de consumo a diferencia de Walmart, que su fuerte es la venta de productos de consumo diario, una diferencia ganadora para Walmart es que, cuentan con centro de ventas en distintos puntos del país o países, dándole la posibilidad al cliente de escoger los productos y conocerlos si así lo requieren

#### 3-13 ¿Qué rol desempeña la tecnología de la información en cada uno de estos negocios? ¿Cómo les ayuda a refinar sus estrategias de negocios?
>##### Ambas empresas se encuentran en desarrollo constante gracias a la evolución tecnológica constante, ya que ambos ofrecen facilidad de compras, plataformas seguras y puntos de entrega eficientes para realizar compras

#### 3-14 ¿Tendrá éxito Walmart contra Amazon.com? Explique su respuesta.
>##### Si, actualmente Walmart ofrece distintas formas de entrega realizando compras en línea, ya sea para recibir directamente en casa o solo para pasar recogiendo el producto fuera de la tienda, a diferencia de Amazon que únicamente ofrece venta en línea y el tiempo de entrega puede ser más largo debido a que no cuentan con centros de venta cercanos
